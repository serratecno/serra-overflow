import Debug from 'debug';
import app from './app';
import mongoose from 'mongoose';
import { mongoUrl, port } from './config';

const debug = new Debug('serra-overflow:root');

mongoose.Promise = global.Promise;

async function start() {
  mongoose.set('useCreateIndex', true);
  await mongoose.connect(mongoUrl, { useNewUrlParser: true });

  app.listen(port, () => {
    debug(`Server running at port ${port}`);
  });
}

start();
